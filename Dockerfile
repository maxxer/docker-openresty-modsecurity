FROM ubuntu:18.04 AS modsecurity-build
LABEL maintainer="Lorenzo Milesi <lorenzo@mile.si>"

# Install Prereqs
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -qq && \
    apt-get install -qq -y --no-install-recommends --no-install-suggests \
    ca-certificates      \
    automake             \
    autoconf             \
    build-essential      \
    libcurl4-openssl-dev \
    libpcre++-dev        \
    libtool              \
    libxml2-dev          \
    libyajl-dev          \
    lua5.2-dev           \
    git                  \
    pkgconf              \
    ssdeep               \
    libgeoip-dev         \
    wget             &&  \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN cd /opt && \
    git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity && \
    cd ModSecurity && \
    git submodule init && \
    git submodule update && \
    ./build.sh && \
    ./configure && \
    make -j 4 && \
    make install

RUN strip /usr/local/modsecurity/bin/* /usr/local/modsecurity/lib/*.a /usr/local/modsecurity/lib/*.so*


FROM ubuntu:18.04 AS openresty-build

ARG OPENRESTY_VERSION=1.15.8.2
ARG OPENSSL_VERSION=1.1.1c
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -qq && \
apt-get install -qq -y --no-install-recommends --no-install-suggests \
    ca-certificates \
    autoconf        \
    automake        \
    build-essential \
    libtool         \
    pkgconf         \
    curl            \
    git             \
    zlib1g-dev      \
    libpcre3-dev    \
    libxml2-dev     \
    libyajl-dev     \
    lua5.2-dev      \
    libgeoip-dev    \
    libcurl4-openssl-dev

RUN cd /opt && \
    git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git

RUN cd /opt && \
    git clone --recursive https://github.com/google/ngx_brotli.git

COPY --from=modsecurity-build /usr/local/modsecurity/ /usr/local/modsecurity/

RUN mkdir -p /opt/openresty
RUN curl -Ls https://github.com/openresty/openresty/releases/download/v"$OPENRESTY_VERSION"/openresty-"$OPENRESTY_VERSION".tar.gz | \
    tar -xz --strip 1 -C /opt/openresty
RUN curl -s https://www.openssl.org/source/openssl-"$OPENSSL_VERSION".tar.gz | tar -xz -C /opt

RUN cd /opt/openresty && \
./configure \
        --prefix=/usr/local/nginx \
        --sbin-path=/usr/local/nginx/bin \
        --modules-path=/usr/local/nginx/modules \
        --conf-path=/etc/nginx/nginx.conf \
        --error-log-path=/var/log/nginx/error.log \
        --http-log-path=/var/log/nginx/access.log \
        --pid-path=/run/nginx.pid \
        --lock-path=/var/lock/nginx.lock \
        --user=www-data \
        --group=www-data \
        --with-pcre-jit \
        --with-file-aio \
        --with-threads \
        --with-http_addition_module \
        --with-http_auth_request_module \
        --with-http_flv_module \
        --with-http_gunzip_module \
        --with-http_gzip_static_module \
        --with-http_mp4_module \
        --with-http_random_index_module \
        --with-http_realip_module \
        --with-http_slice_module \
        --with-http_ssl_module \
        --with-http_sub_module \
        --with-http_stub_status_module \
        --with-http_v2_module \
        --with-http_secure_link_module \
        --with-stream \
        --with-stream_realip_module \
        --add-module=/opt/ModSecurity-nginx \
        --add-module=/opt/ngx_brotli \
        --with-openssl=../openssl-"$OPENSSL_VERSION" \
        --with-openssl-opt=no-nextprotoneg \
        --with-cc-opt='-g -O2 -specs=/usr/share/dpkg/no-pie-compile.specs -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' \
        --with-ld-opt='-specs=/usr/share/dpkg/no-pie-link.specs -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie' \
        --with-http_dav_module

RUN cd /opt/openresty && \
    make -j 4 && \
    make install

###
### Build final image
###
FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive
## Version for ModSecurity Core Rule Set
ARG CRS_VERSION=3.1.1


# Libraries for ModSecurity
RUN apt-get update && \
    apt-get install --no-install-recommends --no-install-suggests -y \
    ca-certificates \
    libcurl4-openssl-dev  \
    libluajit-5.1-2 \
    libyajl-dev \
    lua5.2-dev \
    libgeoip-dev \
    vim \
    libxml2 \
    software-properties-common \
    curl 
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY --from=modsecurity-build /usr/local/modsecurity/ /usr/local/modsecurity/
RUN ldconfig

COPY --from=openresty-build /usr/local/nginx/bin/nginx /usr/local/nginx/bin/nginx
COPY --from=openresty-build /etc/nginx /etc/nginx
COPY --from=openresty-build /usr/local/nginx/nginx/html /usr/local/nginx/html
COPY --from=openresty-build /usr/local/nginx/luajit/lib/ /usr/local/nginx/luajit/lib
COPY --from=openresty-build /usr/local/nginx/lualib/ /usr/local/nginx/lualib

## Get ModSecurity CRS
RUN mkdir -p /etc/nginx/modsecurity.d/owasp-crs
RUN curl -s https://codeload.github.com/SpiderLabs/owasp-modsecurity-crs/tar.gz/v"$CRS_VERSION" | \
    tar -xvz --strip 1 -C /etc/nginx/modsecurity.d/owasp-crs
COPY --from=modsecurity-build /opt/ModSecurity/unicode.mapping /etc/nginx/modsecurity.d/

## Update nginx config
COPY nginx /etc/nginx/
RUN mkdir -p /data/nginx
# FIXME this needs to be taken down one level
RUN mkdir -p /usr/local/nginx/nginx
RUN mkdir -p /var/log/nginx/
RUN touch /var/log/nginx/access.log
RUN touch /var/log/nginx/error.log
RUN openssl dhparam -out /etc/nginx/dhparam.pem 4096

# Letsencrypt
RUN apt-get -y install software-properties-common
RUN add-apt-repository -y ppa:certbot/certbot
RUN apt-get -y install certbot
# Reload openresty after cert renewal
RUN mkdir -p /etc/letsencrypt/renewal-hooks/post/
RUN printf '#!/bin/sh\n/usr/local/nginx/bin/nginx -s reload\n' > /etc/letsencrypt/renewal-hooks/post/reload-openresty.sh
RUN chmod +x /etc/letsencrypt/renewal-hooks/post/reload-openresty.sh

EXPOSE 80
EXPOSE 443

STOPSIGNAL SIGTERM

CMD ["/usr/local/nginx/bin/nginx", "-g", "daemon off;"]
