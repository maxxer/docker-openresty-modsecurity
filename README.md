# WAF with OpenResty + libModSecurity + CRS3 + Letsencrypt

High performance Web Application Firewall docker image.

## Components

Openresty is built from source with Gzip and Brotli compression enabled by default 
(see [nginx.conf](nginx/nginx.conf)), and ModSecurity.

Latest OWASP Core Rule Set is installed inside container in `/etc/nginx/modsecurity.d`.

The usual `crs-setup.conf` is in `/etc/nginx/modsecurity.d/dom_crs-setup.conf` inside the container.

## Usage

To run the default 
```bash
docker run -d -p 80:80 -p 443:443 --restart=unless-stopped --name CONTAINERNAME -v /data/waf/nginx/:/data/nginx/ maxx3r/openresty-modsecurity
```

By mounting volume `/data/nginx` to a local path (`/data/waf/nginx/` in the example above) you can add whatever configuration you want to openresty. 
The default configuration will load any `*.conf` file in that path, so you can add
subdirectories for logging or other files like certificates or custom rules.

You can find a proxy virtualhost configuration in [virtualhost-proxy.example](examples/virtualhost-proxy.example).

Once deployed in the mounted dir you can test openresty config with:

```bash
docker exec -it CONTAINERNAME /usr/local/nginx/bin/nginx -t
```

and finally apply it with:

```bash
docker exec -it CONTAINERNAME /usr/local/nginx/bin/nginx -s reload
```

## Timezone

If you want correct timestamps in log files run the docker command with `-e TZ=Europe/Rome`, obviously replacing the value with your TZ. You can get a list of possible
timezones [on wikipedia](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List).

## Additional configurations

There are some webserver configuration presets available for you to be included, to enhance security in general
or for specific applications. These can be included in your virtualhost with `include /etc/nginx/extra/{file}.conf`.

*  [cipherlist.conf](extra/cipherlist.conf): strong ciphers from [cipherli.st](https://cipherli.st/)
*  [cloudflare.conf](extra/cloudflare.conf): log original visitor IP if [behind Cloudflare](https://support.cloudflare.com/hc/en-us/articles/200170786-Restoring-original-visitor-IPs-Logging-visitor-IP-addresses-with-mod-cloudflare-)

## Letsencrypt certificates

If you want to use Letsencrypt certificates you need to mount container's `/etc/letsencrypt` on the host filesystem (`-v`).

The nginx plugin doesn't play nice with packages built from source, so you'll need to use the webroot one. In your domain config you must add a root
for verification (if you don't have a webroot yourself) and then obtain the cert with:

```bash
docker exec -it CONTAINERNAME certbot certonly --webroot -w /usr/local/nginx/html/ -d YOUR.DOMAIN.NAME
```

Then add the newly released certs to your host config and reload openresty.

There's a script which automatically reloads openresty when a LE certificate has been renewed.

## Log rotation

If you intend to keep openresty log into the host machine you will need log rotation. As an example
you can create `/etc/logrotate.d/waf` with the following content (adjust to your setup):

```
/data/waf/nginx/log/*.log {
        daily
        missingok
        rotate 14
        compress
        delaycompress
        notifempty
        sharedscripts
        postrotate
                docker exec -it CONTAINERNAME /bin/sh -c 'kill -s USR1 $(cat /run/nginx.pid)' >/dev/null 2>&1
        endscript
}
```
Adjust paths and retention to your needs.

## Building

If you want to make changes to the `Dockerfile` and want to build the image yourself just run:

```bash
docker build -t maxx3r/openresty-modsecurity .
```

## Credits

This image is largely *inspired* from [krish512/modsecurity](https://github.com/krish512/docker_nginx_modsecurity).

WAF setup partially based on [Rev3rse Security](https://www.youtube.com/watch?v=1IdGTtPNQy0&list=PLU-OXOuWa-lnjk4bHI3SF9e1HrUiXQL5G&index=1) WAF video tutorial (in italian)
